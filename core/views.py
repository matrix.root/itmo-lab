from django.shortcuts import render, redirect, get_object_or_404

from .models import Worker, Departament, EmploymentContract, Training
from .forms import LoginForm, DepartamentForm, WorkerForm, DepartamentWorkerAdderForm, TrainingForm
from .helpers import get_worker

id_key = 'id'
birthday_key = 'birthday'
logged_in = 'logged_in'


def fill_context_for_logged(context):
    context[logged_in] = True
    context['training_creator'] = TrainingForm()
    context['worker_form'] = WorkerForm()
    context['worker_list'] = Worker.objects.all()
    for worker in context['worker_list']:
        worker.form = WorkerForm(instance=worker)
        worker.trainings = Training.objects.filter(worker=worker)


def add_departament_worker(request, departament_id):
    form = DepartamentWorkerAdderForm(request.POST)
    if form.is_valid():
        form.cleaned_data['worker'] = form.cleaned_data['worker'][0]
        EmploymentContract.objects.create(**form.cleaned_data, departament_id=departament_id)
    return redirect('index')


def fill_departament_data(context):
    for departament in context['departament_list']:
        departament.workers = [{
            'worker': employment.worker,
            'is_head': employment.is_head,
            'editor': WorkerForm(instance=employment.worker)
        } for employment in departament.employmentcontract_set.all()]
        departament.worker_adder = DepartamentWorkerAdderForm(workers=[
            worker_data['worker'] for worker_data in departament.workers
        ])
        departament.form = DepartamentForm(instance=departament)


def fill_context_for_super(context):
    fill_context_for_logged(context)
    context['is_super'] = True
    context['departament_form'] = DepartamentForm()
    context['departament_list'] = Departament.objects.all()
    fill_departament_data(context)


def delete_departament_worker(request, departament_id, worker_id):
    EmploymentContract.objects.filter(departament_id=departament_id, worker__id=worker_id).delete()
    return redirect('index')


def fill_context_for_head(context, head_contracts):
    fill_context_for_logged(context)
    context['departament_list'] = [contract.departament for contract in head_contracts]
    fill_departament_data(context)


def index(request):
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            request.session[id_key] = form.cleaned_data[id_key]
            request.session[birthday_key] = form.cleaned_data[birthday_key]
    else:
        form = LoginForm()

    context = {'login_form': form, 'super_user': Worker.objects.filter(is_super=True).first()}

    try:
        worker = get_worker(request.session[id_key], request.session[birthday_key])
        head_contracts = worker.employmentcontract_set.filter(is_head=True)
        context['user'] = worker
        if worker.is_super:
            fill_context_for_super(context)
        elif head_contracts:
            fill_context_for_head(context, head_contracts)
    except (KeyError, Worker.DoesNotExist):
        context[logged_in] = False

    return render(request, 'index.html', context)


def logout(request):
    request.session.clear()
    return redirect('index')


def create_departament(request):
    if request.POST:
        form = DepartamentForm(request.POST)
        if form.is_valid():
            Departament.objects.create(**form.cleaned_data)
    return redirect('index')


def add_training(request, worker_id):
    if request.POST:
        form = TrainingForm(request.POST)
        if form.is_valid():
            Training.objects.create(worker_id=worker_id, **form.cleaned_data)
    return redirect('index')


def delete_departament(request, departament_id):
    get_object_or_404(Departament, id=departament_id).delete()
    return redirect('index')


def update_departament(request, departament_id):
    instance = get_object_or_404(Departament, id=departament_id)
    form = DepartamentForm(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
    return redirect('index')


def create_worker(request):
    if request.POST:
        form = WorkerForm(request.POST)
        if form.is_valid():
            Worker.objects.create(**form.cleaned_data)
    return redirect('index')


def delete_worker(request, worker_id):
    get_object_or_404(Worker, id=worker_id).delete()
    return redirect('index')


def update_worker(request, worker_id):
    instance = get_object_or_404(Worker, id=worker_id)
    form = WorkerForm(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
    return redirect('index')


def delete_training(request, training_id):
    Training.objects.get(id=training_id).delete()
    return redirect('index')
