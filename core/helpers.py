from .models import Worker


def get_worker(worker_id, birthday):
    return Worker.objects.get(id=worker_id, birthday=birthday)
